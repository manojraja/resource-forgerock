package com.example.oidc.forgerock;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins="http://localhost:4200", methods = {RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST} )

public class HomeController {
	@GetMapping("/")
	public Principal user(Principal principal) {
		return principal;
	}
	
	@GetMapping("/products")
	public List<String> getProducts() {
		List<String> response = new ArrayList<String>();
		response.add("Cars");
		response.add("Chocolates");
		response.add("Coffee");
		
		return response;
	}

}
